# Question
We saw that for the case of sending one packet from source to destination over a path consisting of `N` links each of rate `R` (thus, there are `N-1` routers between source and destination), the end-to-end delay is: `NL/R`.

You may now want to try to determine what the delay would be for `P` packets sent over a series of `N` links.

# Answer
As we saw with the example, when `N=2` and `P=3` there are two general steps:
- The last packet will start being sent when all the previous packets have reached the first router: `2L/R`.
- When it starts being sent, the last packet will reach the destination in `2L/R`.
The total is `4L/R`.

Lets add another link: `N=3`, `P=3`:
- The last packet will start being sent when all the previous packets have reached the first router: `2L/R -> (P-1)L/R`.
- When it starts being sent, the last packet will reach the destination in `3L/R -> NL/R`.
The total is `5L/R`. And we can see that the delay is `d=(P+N-1)L/R`.

