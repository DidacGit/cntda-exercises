# Question

If `N` packets of arrive at the same time, while the transmission delay is `dt=L/R`, what is the average queuing delay?

# Answer
Let's take a couple of example scenarios.
For all packets, `dt=L/R`

- `N=2`
  1. First Packet: `dq=0`.
  2. Second Packet: `dq=dt=L/R`

`A = (L/R)/N = L/2R`

- `N=3`
  1. First Packet: `dq=0`.
  2. Second Packet: `dq=dt=L/R`
  3. Third Packet: `dq=2dt=2L/R`

`A = (L/R + 2L/R)/N = L/R`

We can see how:
`A = (sum(i=1, N-1) NL/R ) / N = L/NR * sum(i=1, N-1)`
