# Question 1
If we have the equation of `dend-end=N(dproc + dtrans + dprop)`, where `dtrans=L/R` and there are `N-1` routers between the source and the destination. Generalize it to heterogeneous delays at the nodes (and links, I assume).

# Answer 1
- dproc, like dtrans, is a function of the bits that enter the node and the node's processing rate: `R' (bits/sec)`.
  - `dtrans = sum(from i=1 to N) L/Ri`. Since the source has to transmit it but the destination doesn't.
  - `dproc = sum(from i=1 to N) L/R'i`. Since the destination has to process it but the source doesn't.
- dprop is a function of the length of the link `D (m)` and the link speed `S (m/s)`. Also, there are `N` links: `dprop = sum(from i=1 to N) Di/Si`.

In total: `dend-end = L*(sum{1/Ri} + sum{1/R'i}) + sum{Di/Si}`.

# Question 2
Add also the presence of an average queuing delay at each node.

# Answer 2
Note that the question only tells us about the presence of an average queuing delay at each node. This is then as straightforward as: 
`dqueue=sum(from i=1 to N) dqueuei`
Therefore, the total will be:
`dend-end = L*(sum{1/Ri} + sum{1/R'i}) + sum{Di/Si} + sum{dqueuei}`.