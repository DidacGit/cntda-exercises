# Question
In the above problem, suppose `R1=R2=R3=R` and `dproc=0`. Further suppose the packet switch does not store-and-forward packets but instead immediately transmits each bit it receives before waiting for the entire packet to arrive. What is the end-to-end delay?

# Answer
If the packet-switches don't introduce transmission delay, it will be only be `L/R`.

`d-end-to-end = L/R + 1/s*(d1 + d2 + d3) = 46 msec`.
