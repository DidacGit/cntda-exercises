# Question
A packet switch receives a packet and determines the outbound link to which the packet should be forwarded. When the packet arrives, one other packet is halfway done being transmitted on this outbound link and four other packets are waiting to be transmitted. Packets are transmitted in order of arrival. Suppose all packets are 1,500 bytes and the link rate is 2 Mbps. What is the queuing delay for the packet? More generally, what is the queuing delay when all packets have length L, the transmission rate is R, x bits of the currently-being-transmitted packet have been transmitted, and n packets are already in the queue?

# Answer
The queuing delay for the packet is the sum of the transmission delays of the packets in front of it:

`dqueue =1/2L/R + 4L/R = 9/2LR = 27 msec`

For all those variables, the queuing delay is:
`dqueue = (L-x)/R + nL/R`