# Question
Consider the queuing delay in a router buffer. Let I denote traffic intensity; that is, I=La/R. Suppose that the queuing delay takes the form IL/R(1−I) for I<1.

- a. Provide a formula for the total delay, that is, the queuing delay plus the transmission delay.
- b. Plot the total delay as a function of L /R.

# Answer

## a
`dtot = dqueue + dtrans = (L*I)/R(1-I) + L/R = L/R * (1 + I/1-I) = L/R(1-I)`

## b
`dtot = L/R(1-I) = L/R(1-La/R) = x/(1-ax)`
Which, for `I<1`:
- At `x=0`, `dtot=0`
- It keeps increasing towards an asymptote (infinite `dtot`) as it reaches `x=1/a`. 