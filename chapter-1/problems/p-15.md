# Question
Let a denote the rate of packets arriving at a link in packets/sec, and let µ denote the link’s transmission rate in packets/sec. Based on the formula for the total delay (i.e., the queuing delay plus the transmission delay) derived in the previous problem, derive a formula for the total delay in terms of a and µ.

# Answer
We know that the transmission delay is given by `L (bit/pack)` and `R (bit/sec)`: `L/R (sec/pack)`. Therefore `L/R = 1/µ`.

`dtot = (L/R) / (1 - aL/R) = 1 / µ(1- a/µ) = 1/(µ-a)`