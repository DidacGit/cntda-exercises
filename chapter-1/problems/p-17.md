# Question
- a. Generalize Equation 1.2 in Section 1.4.3 for heterogeneous processing rates, transmission rates, and propagation delays.
- b. Repeat (a), but now also suppose that there is an average queuing delay of dqueue at each node.

# Answer
This is responded in the in-chapter exercise: [page-69-total-delay.md](../in-chapter-exercises/page-69-total-delay.md).