# Question
- a. Visit the site www.traceroute.org and perform traceroutes from two different cities in France to the same destination host in the United States. How many links are the same in the two traceroutes? Is the transatlantic link the same?
- b. Repeat (a) but this time choose one city in France and another city in Germany.
- c. Pick a city in the United States, and perform traceroutes to two hosts, each in a different city in China. How many links are common in the two traceroutes? Do the two traceroutes diverge before reaching China?

# Answer
After doing the test with multiple cities within my own country and the other tests through a transatlantic link I observed that:
- Normally at maximum, there's only one link in common.
- The transatlantic link is normally not the same.
