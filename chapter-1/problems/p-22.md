# Question
Consider Figure 1.19(b). Suppose that each link between the server and the client has a packet loss probability p, and the packet loss probabilities for these links are independent. What is the probability that a packet (sent by the server) is successfully received by the receiver? If a packet is lost in the path from the server to the client, then the server will re-transmit the packet. On average, how many times will the server re-transmit the packet in order for the client to successfully receive the packet?

# Answer

## Probability that a packet (sent by the server) is successfully received by the received
If there are N links:
`P = (1-p)^N`

## Re-transmissions
- If it has, e.g.:
  - P=1/2, it will be re-transmitted on average: 1 time
  - P=1/3, it will be re-transmitted on average: 2 times
  
Therefore: `# of re-transmissions = (1/P) - 1`.
