# Question

In modern packet-switched networks, including the Internet, the source host segments long, application-layer messages (for example, an image or a music file) into smaller packetsand sends the packets into the network. The receiver then reassembles the packets back into  the original message. We refer to this process as message segmentation. Figure 1.27 illustrates the end-to-end transport of a message with and without message segmentation. Consider a message that is 8⋅106 bits long that is to be sent from source to destination in Figure 1.27 . Suppose each link in the figure is 2 Mbps. Ignore propagation, queuing, and processing delays.

- a. Consider sending the message from source to destination without message segmentation. How long does it take to move the message from the source host to the first packet switch? Keeping in mind that each switch uses store-and-forward packet switching, what is the total time to move the message from source host to destination host?

- b. Now suppose that the message is segmented into 800 packets, with each packet being 10,000 bits long. How long does it take to move the first packet from source host to the first switch? When the first packet is being sent from the first switch to the second switch, the second packet is being sent from the source host to the first switch. At what time will the second packet be fully received at the first switch?

- c. How long does it take to move the file from source host to destination host when message segmentation is used? Compare this result with your answer in part (a) and comment.

- d. In addition to reducing delay, what are reasons to use message segmentation?

- e. Discuss the drawbacks of message segmentation.

# Answer

## a

```
d = 3 dtrans = 3 L/R = 3 * 8*10^6b * 1s/2*10^6b = 12s
```

## b

```
d = L/R = 10*10^3b * 1s/2*10^6b = 5*10^-3s

t = d -> The first packet is fully in the first switch, the second starts to be sent
t = 2d -> the second is fully received at the first switch
t = 2d = 2 * 5*10^-3 = 10^-2s
```

## c

1. In the first link, bits are continuously transmitted: `d1 = L/R`.
2. Then, the last packet is transmitted through the other two links = `d2 = 2L'/R`.

```
d = L/R + 2L'/R = 8*10^6b * 1s/2*10^6b + 2 * 10^4b/2*10^6b = 4.01s
```

It's almost 3 times faster with message segmentation. Meaning that, with small enough packets, message segmentation reduces the total delay that of the first link.

## d

If there's any problem like packet loss, there will be less loss of data. Also, routers would require much higher memory to store big packets. And these would halt the routers during a lot of time and other, smaller, packets would not be sent.

## e

It requieres two extra steps of disassembling the message into packets and then reassembling them. Since all the packets would have the same amount of space reserved for the header, the total would be greater.