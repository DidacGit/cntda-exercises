# Question

Consider sending a large file of F bits from Host A to Host B. There are three links (and two switches) between A and B, and the links are uncongested (that is, no queuing delays). Host Asegments the file into segments of S bits each and adds 80 bits of header to each segment, forming packets of L=80 + S bits. Each link has a transmission rate of R bps. Find the value of S that minimizes the delay of moving the file from Host A to Host B. Disregard propagation delay.

# Answer

![](/home/didac/src/CNTDA-exercises/chapter-1/problems/p-33.jpg)