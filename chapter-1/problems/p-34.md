# Question

Skype offers a service that allows you to make a phone call from a PC to an ordinary phone. This means that the voice call must pass through both the Internet and through a telephone network. Discuss how this might be done.

# Answer

There has to be an intermediary adapter: an end system (Skype server) that transforms the packet switched signal to a circuit switched one, and vice-versa.