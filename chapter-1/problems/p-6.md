# Question
This elementary problem begins to explore propagation delay and transmission delay, two central concepts in data networking. Consider two hosts, A and B, connected by a single link of rate R bps. Suppose that the two hosts are separated by m meters, and suppose the propagation speed along the link is s meters/sec. Host A is to send a packet of size L bits to Host B.

- a. Express the propagation delay, dprop, in terms of m and s.
- b. Determine the transmission time of the packet, dtrans, in terms of L and R.
- c. Ignoring processing and queuing delays, obtain an expression for the end-to-end delay.
- d. Suppose Host A begins to transmit the packet at time t=0. At time t= dtrans, where is the last bit of the packet?
- e. Suppose dprop is greater than dtrans. At time t=dtrans, where is the first bit of the packet?
- f. Suppose dprop is less than dtrans. At time t=dtrans, where is the first bit of the packet?
- g. Suppose s=2.5⋅108, L=120 bits, and R=56 kbps. Find the distance m so that dprop equals dtrans.

# Answer

## a
`dprop = m/s (sec)`

## b
`dtrans = L/R (sec)`

## c
`d-end-to-end = dtrans + dprop = L/R + m/s (sec)`

## d
The last bit of the packet has just been transmitted, therefore it's at the beginning of the link: 0 meters.

## e
The first bit of the packet is still traveling through the link and hasn't reached the host B yet.

## f
The first bit of the packet has already reached host B.

## g
`L/R = m/s; m = Ls/R = (120bit * 2.5*10^8 m/s) / (56*10^3 bit/s) = 5.36*10^5 m`