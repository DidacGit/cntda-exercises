# Question
Describe the most popular wireless Internet access technologies today. Compare and contrast them.

# Answer

## WiFi
To use it, the user needs to be within a few tens of meters of the access point. Therefore, it's mostly used in buildings like homes, universities, cafes, etc. It can provide up to more than 100 Mbps.

## Wide-Area Wireless Access
These, as the name indicates, enable the user to be in a greater range: a few tens of kilometers of the base station. The Internet access speeds of them are up to:

- 3G: 1-7 Mbps
- LTS (4G): 10-100 Mbps
- 5G: 1 Gbps