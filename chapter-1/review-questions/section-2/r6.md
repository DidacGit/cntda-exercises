# Question
List the available residential access technologies in your city. For each type of access, provide the advertised downstream rate, upstream rate, and monthly price.

# Answer

| Access Technology | Downstream rates up to | Upstream reates up to | Monthly prices |
|-------------------|------------------------|-----------------------|----------------|
| (A)DSL            | 20 Mbps                | 1 Mbps                | 20-30€         |
| Cable             | 300 Mbps               | 50 Mbps               | 30-50€         |
| Fiber Optic       | 1 Gbps                 | 500 Mbps              | 30-50€         |
