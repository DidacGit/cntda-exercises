# Question
What is the transmission rate of Ethernet LANs?

# Answer
For home Ethernet access it's usually 100 Mbps or 1 Gbps, whereas servers may have 1 Gbps up to 10 Gbps.