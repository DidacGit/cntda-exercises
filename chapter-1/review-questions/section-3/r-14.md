# Question
Why will two ISPs at the same level of the hierarchy often peer with each other? How does an IXP earn money?

# Answer
They may use IXP to bypass the need of using higher level ISPs. I guess they make money by charging a fee on ISPs, smaller than the ones that higher level ISPs charge.