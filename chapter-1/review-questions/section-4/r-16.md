# Question
Consider sending a packet from a source host to a destination host over a fixed route. List the delay components in the end-to-end delay. Which of these delays are constant and which are variable?

# Answer
These are the 4 types of delays and its components:

## Processing Delay
When a packet reaches a node, the node has to process its header and determine where to direct it. It is a function of the nodes' processing rate and the packet size. It is therefore **constant**.

## Queuing Delay
 After being processed and before being transmitted, the packet might need to wait in a buffer of earlier-arriving packets. This depends on the traffic of the network. It is a function of the node's processing delay and the amount of packets waiting in the queue. It is therefore **variable**.

## Transmission Delay
After the packet is processed, it has to be sent to the link. It is a function of the link's transmission rate and the packet size. It is therefore **constant**.

## Propagation Delay
Then the bits are sent through the link. It is simply a function of the link's propagation speed and the distance to the next node. It is therefore **constant**.