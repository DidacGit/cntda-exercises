# Question
Which layers in the Internet protocol stack does a router process? Which layers does a link-layer switch process? Which layers does a host process?

# Answer

## Router
- Network
- Link
- Physical

## Link-layer switch
- Link
- Physical

## End Host
All of them.
